$(document).ready(function() {
   TablaDePacientes();




      $('#nuevo_pa').submit(function (ev) {
  $.ajax({
    type: $('#nuevo_pa').attr('method'), 
    url: $('#nuevo_pa').attr('action'),
    data: $('#nuevo_pa').serialize(),
    success: function (data) {


      alertify.set('notifier','position', 'bottom-center'); 

      if (parseInt(data) == 1) {
      alertify.success('Registro Exitoso').dismissOthers(); 
      document.getElementById("nuevo_pa").reset();
      TablaDePacientes();
      }else{
      alertify.error('Error en Registro').dismissOthers(); 
      }



      } 
  });
  ev.preventDefault();
});

 


});

function TablaDePacientes(){
 responsive: true;

  $('#TablaPacientes').DataTable({
   "language": {
         "sProcessing": "Procesando...",
         "sLengthMenu": "Mostrar _MENU_ registros",
         "sZeroRecords": "No se encontraron resultados",
         "sEmptyTable": "Ningún dato disponible en esta tabla",
         "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix": "",
         "sSearch": "Buscar:",
         "sUrl": "",
         "sInfoThousands": ",",
         "sLoadingRecords": "Cargando...",
         "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
         },
         "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
         }
      },
      "destroy": true,
      "responsive": true,
      "paging": true,
      "autoWidth": false,
      "ajax": {
      "url": "../../api/MostrarPacientes.php",
      "type": "GET",
      "dataSrc":"",
      },
      "columns": [{
         data: "nombre_pa"
              },
     {
          data: "edad_pa"
              }
              ,
     {
          data: "representante_pa"
              }
              ,
     {
          data: "tlf_pa"
              }
              ,
     {
          data: "dir_pa"
              }
       ]
   });

}