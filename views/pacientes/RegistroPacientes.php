<?php 
include('../../default/header.php');
?>



<div class="card bg-light mb-3 p-3">
	  <div class="row" style="margin-bottom: 2em;">
	  	<center><h3>REGISTRO Y CONTROL DE PACIENTES</h3></center>
    

  </div>

<form class="row g-3 needs-validation" novalidate id="nuevo_pa" method="POST" action="../../api/GuardarCliente.php">
  <div class="col-md-4">
    <label for="nombre_pa" class="form-label">Niño</label>
    <input type="text" class="form-control" id="nombre_pa" name="nombre_pa" placeholder="Pedro Perez" required>
    <div class="valid-feedback">
      Looks good!
    </div>
  </div>
  <div class="col-md-1">
    <label for="edad_pa" class="form-label">Edad</label>
    <input type="number" class="form-control" id="edad_pa" name="edad_pa"placeholder="1" required>
    <div class="valid-feedback">
      Looks good!
    </div>
  </div>
  <div class="col-md-4">
    <label for="representante_pa" class="form-label">Representante</label>
    <div class="input-group has-validation">
      <input type="text" class="form-control" id="representante_pa" name="representante_pa" aria-describedby="inputGroupPrepend" required>
      <div class="invalid-feedback">
        Please choose a username.
      </div>
    </div>
  </div>
    <div class="col-md-3">
    <label for="cedula_re" class="form-label">Cedula</label>
    <div class="input-group has-validation">
      <input type="text" class="form-control" id="cedula_re" name="cedula_re" aria-describedby="inputGroupPrepend" required>
      <div class="invalid-feedback">
        Please choose a username.
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <label for="direccion_pa" class="form-label">Localidad</label>
    <input type="text" class="form-control" id="direccion_pa" name="direccion_pa" required>
    <div class="invalid-feedback">
      Please provide a valid city.
    </div>
  </div>
  <div class="col-md-3">
    <label for="tlf_pa" class="form-label">Telefono</label>
 	<input type="text" class="form-control" id="tlf_pa" name="tlf_pa" required>
    <div class="invalid-feedback">
      Please select a valid state.
    </div>
  </div>
<div class="mb-3">
  <label for="diag_pa" class="form-label">Diagnostico</label>
  <textarea class="form-control" id="diag_pa" name="diag_pa" rows="3"></textarea>
</div>
  <div class="col-12">
    <button class="btn btn-primary" id="guardar_pa" type="submit">Guardar paciente</button>
  </div>
</form>
</div>


<!-- TABLA DE PACIENTES -->
<table id="TablaPacientes" class="table table-striped " style="margin-bottom: 3em;">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Edad</th>
                <th>Representante</th>
                <th>Tlf</th>
                <th>Localidad</th>
              
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Nombre</th>
                <th>Edad</th>
                <th>Representante</th>
                <th>Tlf</th>
                <th>Localidad</th>
            </tr>
        </tfoot>
    </table>




<br>
<br>
<br>



<script src="../../librerias/js_pa.js" type="text/javascript" charset="utf-8" async defer></script>
<?php
include('../../default/footer.php');
?>